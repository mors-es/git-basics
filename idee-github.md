# Formation github

Liens intéressant:

* [free-book programing](https://github.com/vhf/free-programming-books)
* [gitcheat](https://github.com/tiimgreen/github-cheat-sheet)
* [DemocracyOS](http://democracyos.org/), [DemoOSFr](http://democracyos.eu/)
* [Parol](http://parol-production.herokuapp.com/)


![diff-loi](diff-loi.jpg)
![graph-loi](graph-loi.jpg)

* Github: 9 millions d'utilisateurs (2015) (1,4M en 2012), 21,1 millions de projets (2015) (2,4M en 2012). -> grand public
* BitBucket: 2,5M users -> more corporate (2014)
* GitLab: +100k users  -> more "geek"
